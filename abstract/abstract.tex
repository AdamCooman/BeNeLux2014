\documentclass[onecolumn]{blux2014}
\usepackage{mathptmx}
\usepackage[scaled=0.92]{helvet}
\usepackage[pdftex]{graphicx}%
\usepackage{float}
\usepackage{setspace}
\usepackage[caption = false]{subfig}
\graphicspath{{./figs/}}%
\DeclareGraphicsExtensions{.pdf,.jpeg,.png,.jpg}%
\usepackage{courier}

\title{Distortion Contribution Analysis\\by combining the MIMO BLA and noise analysis}
\author{Adam Cooman \\ 
ELEC VUB\\
Adam.Cooman@vub.ac.be
\and
Gerd Vandersteen\\
ELEC VUB\\
Gerd.Vandersteen@vub.ac.be}
\date{}

\begin{document}
\maketitle

To be able to cope with ever more stringent specifications in terms of linearity, designers of analog electronic circuits need tools to effectively reduce the distortion generated in their circuits. A first step in solving this problem is to find the disturbing source. Our goal is to develop a simulation-based method which shows the dominant sources of non-linear distortion in an electronic circuit. To be useful during the design, the analysis should be fast and should avoid the use of special simulation techniques and/or device models.

The method combines the Best Linear Approximation (BLA) with existing noise de-embedding techniques from the microwave measurement community \cite{dobrowolski1989}. The BLA allows to consider the distortion added by every stage as noise. Combining the BLA with a noise analysis allows to pinpoint the dominant source of non-linear distortion \cite{cooman2013}.

\vfill

% ------------------------------------------------------------ figure on the bottom -----------------------------------------------------------------------
\begin{minipage}{\textwidth}
\noindent\rule{\textwidth}{0.4pt}
\begin{center}
\vspace{-0.5em}
\large{\textbf{Summary of the method:}}\\
\end{center}
\includegraphics[width=\textwidth]{explanation2}\\
\begin{minipage}{\textwidth}
\begin{minipage}[t]{0.49\textwidth}
\textbf{1.} The electronic circuit is considered as an interconnected network of non-linear sub-circuits. Each of the blocks will be modelled as a black box.\\
\\
\textbf{2.} The MIMO BLA of the sub-circuits is determined. We use multisines to excite the circuit and measure the voltages and currents at the input and output of each sub-circuit. Using these, we determine the MIMO BLA of each sub-circuit.
\end{minipage}
\hfill
\begin{minipage}[t]{0.49\textwidth}
\textbf{3.} Using the BLA, we determine the distortion introduced by every stage. The distortion sources can be considered as mutually correlated noise sources.\\
\\
\textbf{4.} The contribution of each distortion source in the circuit to the output of the total circuit can be calculated with a classical noise analysis. Comparing the relative contributions allows to pinpoint the dominant sources.
\end{minipage}
\end{minipage}\\
\vspace{0.4cm}\\
% logos and acknowlegements
\begin{minipage}[t]{\columnwidth}
\begin{minipage}{0.25\columnwidth}
\includegraphics[width=\columnwidth]{IWT_logo} 
\end{minipage}
\hfill
\begin{minipage}{0.45\columnwidth}
\begin{spacing}{0.5}
{\scriptsize \vspace{-1.5em}  This  work  is  sponsored  by  the Strategic Research Program of the VUB \mbox{(SRP-19)}, Institute  for  the Promotion of Innovation through Science and Technology in Flanders (IWT-Vlaanderen), Fund for Scientific Research (FWO-Vlaanderen), Flemish Government (Methusalem) and the Belgian Federal Government \mbox{(IUAP VI/4)}}
\end{spacing}
\vfill
\end{minipage}
\hfill
\begin{minipage}{0.25\columnwidth}
\vspace{-1em}\includegraphics[width=\columnwidth]{VUB_logo} 
\end{minipage}
\end{minipage}
\end{minipage}
% ----------------------------------------------------------------------------------------------------------------------------------------------------
\eject

At high frequencies, the input impedance and reverse gain of amplifier stages play an important role. Taking these effects into account requires a port representation of the stages and the interconnection network. This leads to the distortion contribution analysis based on the MIMO BLA \cite{cooman2014}. The different steps of the analysis are described below. This new analysis can be used hierarchically from the system level, down to the transistor level.

{\footnotesize
\begin{thebibliography}{1}
\bibitem{dobrowolski1989}
J.~A.~Dobrowolski, ``A CAD-Oriented Method for Noise Figure Computation of Two-Ports with Any Internal Topology'' \emph{IEEE Tran. on microwave theory and techniques}, vol. 37, no. 1, jan. 1989
\bibitem{cooman2013}
A.~Cooman, G.~Vandersteen and Y.~Rolain. ``Finding the dominant source of  distortion  in  two-stage  op-amps'' \emph{Analog Integrated Circuits and Signal Processing}, vol. 78, no. 1, jan. 2014
\bibitem{cooman2014}
A.~Cooman, G.~Vandersteen. ``Distortion Contribution Analysis by combining the Best Linear Approximation and noise analysis'' \emph{International Symposium in circuits and systems 2014}. To be published.
\end{thebibliography}

}


\vfill




\end{document}